function mine_request(csrf_token, action, count=0, mine_id=0)
{
    $.ajax({
        url: '/game/mine/',
        method: 'post',
        dataType: 'html',
        data: {
            action: action,
            count: count,
            mine_id: mine_id,
            csrfmiddlewaretoken: csrf_token
        },
        success: function(data) {
            location.reload();
            console.log('sss');
        },
        error: function (data) {
            console.log(data);

        }
    });
}

function brigade_create(csrf_token, mine_id)
{
    $.ajax({
        url: '/game/mine/' + mine_id + '/',
        method: 'post',
        dataType: 'html',
        data: {
            action: 'create',
            mine_id: mine_id,
            csrfmiddlewaretoken: csrf_token
        },
        success: function(data) {
            location.reload()
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function brigade_upgrade(csrf_token, mine_id, brigade_id, count)
{
    $.ajax({
        url: '/game/mine/' + mine_id + '/',
        method: 'post',
        dataType: 'html',
        data: {
            action: 'upgrade',
            mine_id: mine_id,
            brigade_id: brigade_id,
            count: count,
            csrfmiddlewaretoken: csrf_token
        },
        success: function(data) {
            location.reload()
        },
        error: function (data) {

        }
    });
}

