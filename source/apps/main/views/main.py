from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView


class MainView(TemplateView):
    """
    Обработка главной страницы
    """
    template_name = "main/main.htm"

    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(MainView, self).get_context_data(**kwargs)
        # Перенаправляем в меню, если уже была выполнена авторизация
        if request.user.is_authenticated:
            return HttpResponseRedirect('/game/')
        return render(request, self.template_name, context)
