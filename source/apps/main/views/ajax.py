from django.apps import apps
from django.http import JsonResponse
from django.views import View

from ...user.models import user
from ...utils.templatetags import short_format


class HeaderUpdateAjax(View):
    """
    Обновляет header
    """
    statistic_model = apps.get_model('user', 'StatisticModel')
    dialog_model = apps.get_model('user', 'DialogModel')
    user_model = apps.get_model('user', 'UserModel')

    def get(self, request):
        """
        Метод получает данные о пользователя из базы и возвращает JSON-ответ
        """
        u_id = request.user.id
        statistic_data = self.statistic_model.get_statistics(self, u_id)

        icon = 'message.png'
        if self.dialog_model.is_read(self, u_id):
            icon = 'message_unread.png'

        data = {
            'budget': short_format.short_format(statistic_data.budget),
            'income': short_format.short_format(statistic_data.income),
            'reputation': statistic_data.reputation,
            'icon': icon
        }

        # Кадое обновление страницы вызвавет обновление статуса
        user.status_update(u_id)



        return JsonResponse(data)


