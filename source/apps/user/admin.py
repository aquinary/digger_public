from django.contrib import admin

from .models import AchievementModel


# Register your models here.
class AchievementAdmin(admin.ModelAdmin):
    list_display = ('id', 'achievement_id', 'level', 'value', 'reward', 'progress', 'user_id')
    search_fields = ('achievement_id', 'user_id')


admin.site.register(AchievementModel, AchievementAdmin)
