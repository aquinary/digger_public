from django.contrib.auth import logout
from django.views.generic import RedirectView


class LogoutView(RedirectView):
    """
    Выход
    """
    url = '/'

    def get(self, request, *args, **kwargs):
        """
        Действия при нажатии кнопки "Выйти"
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)
