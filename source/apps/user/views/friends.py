import time
from pprint import pprint

from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from ...user import services


class FriendsView(TemplateView):
    """
    Вывод списка диалогов
    """

    template_name = "user/friends.htm"
    model = apps.get_model('user', 'FriendsModel')

    @method_decorator(login_required(login_url='/signin/'))
    #@method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        """

        u_id = request.user.id

        context = super(FriendsView, self).get_context_data(**kwargs)

        context['title'] = 'Друзья'

        context['friends'] = self.model.get_list(self, u_id)
        context['friends_in'] = self.model.get_request_in(self, u_id)
        context['friends_out'] = self.model.get_request_out(self, u_id)

        page = self.kwargs.get('page', 1)
        paginator = Paginator([], 5)

        #context['pages'] = paginator.get_page(page)
        #context['page'] = page

        # Код ниже делает так, чтобы пагинация корректно отображалась
        plusator = 3

        if page == 1 or page == paginator.num_pages:
            plusator = 5
        if page == 2 or page == paginator.num_pages - 1:
            plusator = 4
        if page == 3 or page == paginator.num_pages - 2:
            plusator = 3

        context['plusator'] = plusator
        context['minusator'] = -plusator

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        Отправка заявки в друзья
        """
        action = request.POST['action']
        friend = request.POST['friend_id']
        if action == 'request':
            services.RequestFriend.execute({'from_': request.user.id,
                                            'to': friend})
        if action == 'cancel':
            result = self.model.request_cancel(self, request.user.id, friend)
            if result == None:
                messages.error(request, 'Заявка уже не существует!')

        if action == 'accept':
            result = self.model.request_accept(self, request.user.id, friend)
            if result is None:
                messages.error(request, 'Заявка уже не существует!')

        if action == 'delete':
            result = self.model.friend_delete(self, request.user.id, friend)
            if result is None:
                messages.error(request, 'Заявка уже не существует!')


        return HttpResponseRedirect('/friends/')
