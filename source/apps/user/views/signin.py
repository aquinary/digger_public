from django.apps import apps
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from ..forms.signin import SignInForm


class SignInView(TemplateView):
    """
    Обработка страницы авторизации на сайте
    """

    template_name = "user/signin.htm"
    statistic_model = apps.get_model('user', 'StatisticModel')

    #@method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(SignInView, self).get_context_data(**kwargs)

        form = SignInForm()
        context['form'] = form

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = SignInForm(request.POST)

        # Проверка на корректность ввода логина и пароля
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is None:
                messages.error(request, 'Неправильный логин или пароль!')
            else:
                login(request, user)
                return HttpResponseRedirect('/')

        return HttpResponseRedirect('/signin/')