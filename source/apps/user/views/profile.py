from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from ...user.models import user, friend

class ProfileView(TemplateView):
    """
    Вывод профиля пользователя
    """

    template_name = "user/profile.htm"

    friend_model = apps.get_model('user', 'FriendsModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(ProfileView, self).get_context_data(**kwargs)

        context['title'] = 'Профиль'

        u_id = request.user.id
        page = user.get_id(self.kwargs.get('user', None))

        # Проверить, является ли пользователь владельцем аккаунта
        id = u_id if page is None else page

        context['player'] = user.get_info(id)
        context['friend'] = friend.get_friend_status(u_id, context['player'].id)

        return render(request, self.template_name, context)
