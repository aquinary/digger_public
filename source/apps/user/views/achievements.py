from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from ..models import achievement


class AchievementView(TemplateView):
    """
    Вывод списка диалогов
    """

    template_name = "user/achievement.htm"
    model = apps.get_model('user', 'AchievementModel')

    @method_decorator(login_required(login_url='/signin/'))
    #@method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(AchievementView, self).get_context_data(**kwargs)

        context['title'] = 'Достижения'
        achievement.update(request.user.id)
        context['achievements'] = achievement.get(request.user.id)
        return render(request, self.template_name, context)

