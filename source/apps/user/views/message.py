from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from ...utils import utils
from ...user import services
from ..forms import DialogDetailForm


class MessageView(TemplateView):
    """
    Обработка страницы авторизации на сайте
    """

    template_name = "user/message.htm"
    model = apps.get_model('user', 'MessageModel')
    user_model = apps.get_model('user', 'UserModel')

    @method_decorator(login_required(login_url='/signin/'))
    #@method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        """
        context = super(MessageView, self).get_context_data(**kwargs)
        context['title'] = 'Личные сообщения'

        # Скрытое поле, показывающее, кому будет отправлено сообщение
        dialog = self.kwargs.get('dialog', None)
        context['form'] = DialogDetailForm(initial={'addressee': dialog})

        # Для формирования логина и id, если диалогов не было ранее
        context['userinfo'] = self.user_model.get_userinfo(self, dialog)

        # Пагинация на странице
        data = self.model.get_messages(self, request.user.id, dialog)
        context['pages'] = utils.paginator(self, data, 5)

        return render(request, self.template_name, context)

    @method_decorator(login_required(login_url='/signin/'))
    def post(self, request, *args, **kwargs):
        form = DialogDetailForm(request.POST)

        if form.is_valid():
            addressee = request.POST.get('addressee')
            message = request.POST.get('message')

            # Отправляем сообщение другому пользователю
            self.model.send(self, request.user.id, addressee, message)

            services.CreateAction.execute(
                {
                    'from_': request.user.id,
                    'to': addressee,
                    'action': 'Вам пришло <a href="/dialogs/">письмо</a> от игрока <a href="/profile/{player}/">{player}</a>!',
                }
            )

        dialog = self.kwargs.get('dialog', None)
        return HttpResponseRedirect('/dialog/%s/' % dialog)
