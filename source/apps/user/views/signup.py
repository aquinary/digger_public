from django.apps import apps
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from ..forms.signup import SignUpForm


class SignUpView(TemplateView):
    """
    Обработка страницы регистрации на сайте
    """

    template_name = "user/signup.htm"
    user_model = apps.get_model('user', 'UserModel')
    statistic_model = apps.get_model('user', 'StatisticModel')

    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(SignUpView, self).get_context_data(**kwargs)
        context['form'] = SignUpForm()
        return render(request, self.template_name, context)

    def post(self, request, **kwargs):
        """
        Действия при отправка запроса на регистрацию
        :param request:
        :return:
        """
        form = SignUpForm(request.POST)

        # Проверим валидность формы и в случае успеха отправим на страницу входа В противном случае покажем сообщение
        # об ошибке
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            self.user_model.create(self, username, password)

            return HttpResponseRedirect('/signin/')
        else:
            messages.error(request, 'Логин уже занят!')
            return HttpResponseRedirect('/signup/')

        return HttpResponseRedirect('/')
