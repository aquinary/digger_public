from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView


class DialogsView(TemplateView):
    """
    Вывод списка диалогов
    """

    template_name = "user/dialog.htm"
    model = apps.get_model('user', 'DialogModel')

    @method_decorator(login_required(login_url='/signin/'))
    #@method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(DialogsView, self).get_context_data(**kwargs)

        context['title'] = 'Личные сообщения'

        context['messages'] = self.model.get_list(self, request.user.id)

        page = self.kwargs.get('page', 1)
        paginator = Paginator(context['messages'], 5)

        context['pages'] = paginator.get_page(page)
        context['page'] = page

        # Код ниже делает так, чтобы пагинация корректно отображалась
        plusator = 3

        if page == 1 or page == paginator.num_pages:
            plusator = 5
        if page == 2 or page == paginator.num_pages - 1:
            plusator = 4
        if page == 3 or page == paginator.num_pages - 2:
            plusator = 3

        context['plusator'] = plusator
        context['minusator'] = -plusator

        return render(request, self.template_name, context)
