import datetime
import math
from decimal import Decimal

from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView


class StatisticView(TemplateView):
    """
    Вывод списка диалогов
    """

    template_name = "user/statistics.htm"
    model = apps.get_model('user', 'StatisticModel')

    @method_decorator(login_required(login_url='/signin/'))
    ##@method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = super(StatisticView, self).get_context_data(**kwargs)

        context['title'] = 'Статистика'
        context['player'] = self.model.get_statistics(self, request.user.id)


        budget = context['player'].budget
        income = context['player'].income

        # Бюджет
        context['player'].budget1m = budget + (income * 60)
        context['player'].budget1h = budget + (income * 60 * 60)
        context['player'].budget1d = budget + (income * 60 * 60 * 24)

        # Время онлайна
        context['player'].time_online = datetime.datetime.fromtimestamp(
            datetime.datetime.now().timestamp() - context['player'].time_online)


        future_reputation = context['player'].future_reputation
        future_income_reputation = Decimal(100 * math.sqrt((budget + (income * 60)) / 1000000000))
        future_reputation1m = Decimal(100 * math.sqrt((budget + (income * 60)) / 1000000000))
        future_reputation1h = Decimal(100 * math.sqrt((budget + (income * 60 * 60)) / 1000000000))
        future_reputation1d = Decimal(100 * math.sqrt((budget + (income * 60 * 60 * 24)) / 1000000000))
        reputation = Decimal(100 * math.sqrt(budget / 1000000000))

        context['player'].reputation_income = f"{future_income_reputation - reputation:.2f}"
        context['player'].reputation1m = f"{future_reputation + future_reputation1m - reputation:.2f}"
        context['player'].reputation1h = f"{future_reputation + future_reputation1h - reputation:.2f}"
        context['player'].reputation1d = f"{future_reputation + future_reputation1d - reputation:.2f}"

        context['player'].efficiency_time /= 3600
        context['player'].efficiency_recession /= 3600

        context['worker'] = True
        return render(request, self.template_name, context)
