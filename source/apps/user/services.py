import time

from django import forms
from django.apps import apps
from django.db.models import Q
from service_objects.services import Service

class GetUserRewards(Service):
    """
    Получить все награды пользователя
    """
    model = apps.get_model('user', 'RewardModel')
    rewards_models = None

    user_id = forms.CharField()

    def process(self):
        """
        Получить все награды пользователя
        """
        from ..data.rewards import rewards_list
        user_id = self.cleaned_data['user_id']

        # Обращаемся либо по id, либо по username\
        try:
            data = self.model.objects.filter(user_id=user_id)
        except:
            data = self.model.objects.select_related('user').filter(user__username=user_id)

        rewards = []
        for items in data:
            rewards.append(rewards_list[int(items.reward)])

        return rewards


class GetUserInfo(Service):
    """
    Получение информации о пользователе
    user_id - id пользователя
    """
    model = apps.get_model('user', 'UserModel')

    user_id = forms.CharField()

    def process(self):
        """
        Получает всю информацию о пользователе
        """
        user_id = self.cleaned_data['user_id']

        # Обращаемся либо по id, либо по username
        try:
            result = self.model.objects.get(id=user_id)

        except:
            result = self.model.objects.get(username=user_id)

        #print(result.statistics.income)
        return result


class CreateAction(Service):
    """
    Создать действие.
    user_id: пользователь, с котором случилось действие
    action: описание действия
    time: время, когда случилось действие
    """
    model = apps.get_model('user', 'ActionModel')

    from_ = forms.IntegerField()
    to = forms.IntegerField()

    action = forms.CharField()

    def process(self):
        """
        Метод создаёт действие и заносит его в базу
        """

        from_ = int(self.cleaned_data['from_'])
        to = int(self.cleaned_data['to'])
        action = self.cleaned_data['action']

        # Если в тексте встречаем шаблон игрока, то подставляем никнейм
        username = GetUserInfo.execute({'user_id': from_})
        if action.find('{player}'):
            action = action.replace('{player}', username.username)

        result = self.model(owner_id=to, action=action)
        result.save()


class RatingUpdate(Service):
    """
    Обновляет рейтинг игрока по всем параметрам
    TODO: вынести это дело в отдельное место в cron
    """
    model = apps.get_model('user', 'UserModel')

    def process(self):
        """
        Метод получает данные о пользователях, после чего сортирует их. Далее в методе происходит присваивание мест
        На основе присвоенных мест рассчитывается общий рейтинг
        """
        db_data = self.model.objects.all()
        incomes = db_data.order_by('-income')
        budgets = db_data.order_by('-budget')

        # TODO: передаль позже на bulk_update
        # TODO: так же, возможно, существует способ перенести всё это дело в один цикл. Нужно подумать
        for i, income in enumerate(incomes):
            income.income_rating = i + 1
            income.save(update_fields=['income'])

        for i, budget in enumerate(budgets):
            budget.budget_rating = i + 1
            budget.save(update_fields=['budget'])

        # Расчёт score очков
        for score in db_data:
            data_r = [int(score.income_rating),
                      int(score.budget_rating),
                      int(score.ref_rating),
                      int(score.playing_rating),
                      int(score.reputation_rating)]

            score.score = float(sum(data_r) / len(data_r))
            score.save(update_fields=['score'])
        # Подсчитываем места в таблице рейтингов
        for i, rating in enumerate(db_data.order_by('-score')):
            rating.total_rating = i + 1
            rating.save(update_fields=['total_rating'])


class TempUserAddIncome(Service):
    """
    Тестовый класс, для проверки AJAX
    """
    model = apps.get_model('user', 'UserModel')

    user_id = forms.CharField()

    def process(self):
        user_id = self.cleaned_data['user_id']

        result = self.model.objects.get(id=user_id)

        income = int(result.income)
        income += 1
        result.income = income
        result.budget = int(result.budget) + 1000 * result.id
        result.save()

        return result


class GetUnreadDialog(Service):
    """
    Получить информацию о сообщениях
    """
    model = apps.get_model('user', 'MessageModel')

    user_id = forms.CharField()

    def process(self):
        """
        Метод возвращает все непрочитанные соообщения
        """
        user_id = self.cleaned_data['user_id']

        result = self.model.objects.filter(addressee=user_id, status='new')

        return result

class GetFriendsList(Service):
    """
    Получить список друзей
    user_id: id пользователя
    status: from - игрок отправитель
    """
    model = apps.get_model('user', 'FriendsModel')

    user_id = forms.CharField()

    # status = forms.CharField()
    def normalize(self, friends, user_id):
        """
        Метод приводит в порядок список друзей. Он показывает только другие ники, но не наш собственный
        Args:
            friends: список друзей
            user_id: id пользователя

        Returns:
            Нормализованный список словарей
        """
        _friends = []

        for friend in friends:
            print(friend)
            _friend = None
            _id = None
            if friend['from'].id == int(user_id):
                _friend = friend['to']
            else:
                _friend = friend['from']

            _friends.append(
                {
                    'id': _friend.id,
                    'username': _friend,
                    'status': friend['status']
                }
            )

        return _friends

    def process(self):
        """
        Метод возвращает список друзей
        """
        user_id = self.cleaned_data['user_id']

        friends_data = self.model.objects.filter(Q(Q(userfrom=user_id) | Q(userto=user_id)) & Q(status=True))

        friends = []
        for friend in friends_data:
            friends.append({
                'from': friend.userfrom,
                'to': friend.userto,
                'status': friend.status,
            })

        return self.normalize(friends, user_id)


class RequestFriend(Service):
    """
    Запрос в друзья
    """

    model = apps.get_model('user', 'FriendsModel')
    user_model = apps.get_model('user', 'UserModel')
    from_ = forms.CharField()
    to = forms.CharField()

    def process(self):
        from_ = int(self.cleaned_data['from_'])
        to = int(self.cleaned_data['to'])

        # Без этого участка кода ругается на то, что значения id не соответствуют модели UserModel
        # TODO: разобраться в чём дело
        from_ = self.user_model.objects.get(id=from_)
        to = self.user_model.objects.get(id=to)
        result = self.model(friend_id=to.id, user_id=from_.id, status=False)
        result.save()

        return result
