from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('signin/', views.SignInView.as_view(), name="signin"),
    path('signup/', views.SignUpView.as_view(), name="signup"),
    path('profile/', views.ProfileView.as_view(), name="profile"),
    path('profile/<str:user>/', views.ProfileView.as_view(), name="profile"),
    path('dialogs/', views.DialogsView.as_view(), name="dialogs"),
    path('dialogs/<int:page>/', views.DialogsView.as_view(), name="dialogs"),
    path('dialog/<int:dialog>/', views.MessageView.as_view(), name="dialog_detail"),
    path('dialog/<int:dialog>/<int:page>/', views.MessageView.as_view(), name="dialog_detail"),
    path('friends/', views.FriendsView.as_view(), name='friends'),
    path('friends/<int:page>/', views.FriendsView.as_view(), name='friends'),
    path('statistic/', views.StatisticView.as_view(), name='statistic'),
    path('achievement/', views.AchievementView.as_view(), name='achievement'),
    path('logout/', views.LogoutView.as_view(), name="logout"),
]
