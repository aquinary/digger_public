from django.db import models
from django.db.models import Q

from ..models import UserModel

class DialogModel(models.Model):
    """
    Работа со списком диалогов
    Содержит в себе только список диалогов, без самих сообщений

    Поля:
        u_from: от кого было последнее сообщение
        u_to: кому было последнее сообщение
        status: статус прочтения (False - не прочитано, True - прочитано)
    """
    class Meta:
        db_table = 'digger_dialogs'

    u_from = models.ForeignKey(UserModel, related_name='u_from', default=None, null=True, on_delete=models.PROTECT)
    u_to = models.ForeignKey(UserModel, related_name='u_to', default=None, null=True, on_delete=models.PROTECT)
    datetime = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=False)

    def create(self, u_from, u_to):
        """
        Создать новый диалог.
        Если присутствует дубликат, то ничего не создаётся.
        Если присутсвует косвенный дубликат (когда отправители просто меняются местами), то... они меняются местами.

        Аргументы:
            u_from: от кого сообщение
            u_to: к кому сообщение

        Возвращает:
            id созданного диалога
        """
        # Если записи ещё не существует в БД, то создаём её
        model = DialogModel.objects.filter(Q(Q(u_from_id=u_from) & Q(u_to_id=u_to)) | Q(Q(u_from_id=u_to) & Q(u_to_id=u_from))).first()

        if model:
            model.u_from_id = u_from
            model.u_to_id = u_to
            model.status = False
            model.save()
            dialog_id =  model.id
        else:
            model = DialogModel(u_from_id=u_from, u_to_id=u_to)
            model.save()
            dialog_id = model.id

        return dialog_id

    def get_list(self, u_id):
        """
        Вернуть список диалогов пользователя

        Аргументы:
            u_id: id пользователя

        Возвращает:
            Список диалогов пользователя
        """
        model = DialogModel.objects.filter(Q(u_from_id=u_id) | Q(u_to_id=u_id)).order_by('-datetime')

        _dialogs = []

        for dialog in model:
            if dialog.u_from_id == u_id:
                _dialog = dialog.u_to

                icon = 'message_sent.png'
            else:
                _dialog = dialog.u_from

                icon = 'message_received_and_unread.png'
                if dialog.status:
                    icon = 'message_received_and_read.png'

            _dialogs.append(
                {
                    'dialog': _dialog,
                    'time': dialog.datetime,
                    'status': dialog.status,
                    'icon': icon,
                }
            )

        return _dialogs

    def is_read(self, u_id):
        """
        Проверка не прочитанных сообщений
        Args:
            u_id: id пользователя

        Returns:
            True, если в диалогах есть не прочитанные сообщения
            False, если все сообщения прочитаны
        """
        model = DialogModel.objects.filter(Q(u_to_id=u_id), status=False).exists()
        if model:
            return True
        return False

