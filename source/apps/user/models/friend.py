from django.apps import apps
from django.db import models
from django.db.models import Q

from ..models import UserModel


class FriendsModel(models.Model):
    """
    Модель для обработки списков друзей
    from_: тот, кто отправил заявку в друзья
    to: тот, кому отправили заявку в друзья
    status: факт принятия заявки. Если True - то пользователи друзья, если False, то нет
    """

    class Meta:
        db_table = 'digger_friends'

    user = models.ForeignKey(UserModel, related_name='user', default=None, null=True, on_delete=models.PROTECT)
    friend = models.ForeignKey(UserModel, related_name='friend', default=None, null=True, on_delete=models.PROTECT)
    status = models.BooleanField(default=False)

    def request_cancel(self, u_from, u_to):
        """
        Метод отменяет заявку в друзья

        Аргументы:
            u_from: от кого
            u_to: кому

        Вовзращает:
            Ничего
        """
        try:
            try:
                result = FriendsModel.objects.get(user_id=u_from, friend_id=u_to).delete()
            except:
                result = FriendsModel.objects.get(user_id=u_to, friend_id=u_from).delete()
        except:
            return None

        return None

    def request_accept(self, u_from, u_to):
        """
        Метод подтверждения заявки в друзья
        Аргументы:
            u_from: от кого
            u_to: кому
        """
        try:
            result = FriendsModel.objects.get(user=u_from, friend=u_to)
        except:
            result = FriendsModel.objects.get(user=u_to, friend=u_from)

        result.status = True
        result.save()

    def friend_delete(self, u_from, u_to):
        """
        Метод удаления из друзей
        Аргументы:
            u_from: от кого
            u_to: комуs
        """
        try:
            result = FriendsModel.objects.get(user=u_from, friend=u_to)
        except:
            result = FriendsModel.objects.get(user=u_to, friend=u_from)

        result.status = False
        result.save()

    def get_request_in(self, u_id):
        """
        Получить список входящих заявок
        Аргументы:
            u_id: id пользователя

        Возвращает:
            Список входящих заявок
        """
        return FriendsModel.objects.filter(friend=u_id, status=False)

    def get_request_out(self, u_id):
        """
        Получить список исходящих заявок

        Аргументы:
            u_id: id пользователя

        Возвращает:
            Список исходящих заявок
        """
        return FriendsModel.objects.filter(user=u_id, status=False)

    def get_list(self, u_id):
        """
        Получить список друзей
        Аргументы:
            u_id: id пользователя

        Возвращает:
            Список всех друзейss
        """
        # Получаем нужные данные
        friends_data = FriendsModel.objects.filter(Q(Q(user_id=u_id) | Q(friend_id=u_id)) & Q(status=True))

        friends = []
        for friend in friends_data:
            _friend, _id = None, None
            if friend.user.id == u_id:
                _friend = friend.friend
            else:
                _friend = friend.user

            friends.append(
                {
                    'id': _friend.id,
                    'username': _friend,
                    'status': friend.status
                }
            )

        return friends


def get_friend_status(u_id, f_id):
    """
    Метод возвращает словарь состатусом добавления в друзья. Он проверяет, принята ли заявка или нет
    Args:
        f_id: id друга
        u_id: id пользователя

    Returns:
        Словарь состатусом добавления в друзья
    """

    from_to = None
    to_from = None
    try:
        try:
            from_to = FriendsModel.objects.get(Q(user_id=u_id) & Q(friend_id=f_id))
        except:
            to_from = FriendsModel.objects.get(Q(user_id=f_id) & Q(friend_id=u_id))
    except:
        pass

    if from_to is not None:
        friends = {
            'u_from': from_to.user,
            'u_to': from_to.friend,
            'status': from_to.status,
        }
    elif to_from is not None:
        friends = {
            'u_from': to_from.user,
            'u_to': to_from.friend,
            'status': to_from.status,
        }
    else:
        friends = {
            'u_from': None,
            'u_to': None,
            'status': False
        }

    return friends
