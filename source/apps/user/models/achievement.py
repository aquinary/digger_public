import json

from django.db import models

from source.apps.utils.templatetags import short_format
from .user import UserModel

ACHIEVEMENT_JSON = json.load(open('./resources/data/achievement.json', encoding='utf-8'))


class AchievementModel(models.Model):
    """
    Модель, описывающая таблицу достижений для пользователя

    Поля:
        user: ссылка на пользователя

        achievement_id: id на достижение
        progress: прогресс достижения
        level: уровень достижения
        value: значение, которого нужно достичь (100 рабочих, 1000 метров глубины и т.д)
        rewards: награда в алмазах
    """

    class Meta:
        db_table = 'digger_achievements'

    user = models.ForeignKey(UserModel, related_name='achievements', default=None, null=True, on_delete=models.PROTECT)

    achievement_id = models.IntegerField(default=0)
    progress = models.FloatField(default=0)
    level = models.IntegerField(default=1)
    value = models.DecimalField(max_digits=1000, decimal_places=2, default=0)
    reward = models.IntegerField(default=0)


def update(u_id):
    """
    Обновить информацию по достижениям. Так же проверить, выполнено ли какое-либо условие, совпадающее с каким-либо
    достижением
    Args:
        u_id: id пользователя

    Возвращает:
        Ничего
    """
    achievement_data = AchievementModel.objects.filter(user=u_id)

    def _create(u_id):
        """
        Создаёт в таблице список достижений на основе файла достижений
        Если есть новые, то добавляет их
        TODO: возможно, стоит сделать отдельным, при создании самого пользователя
        Args:
            u_id: id пользователя

        Возвращает:
            Ничего
        """
        achievements = ACHIEVEMENT_JSON
        for achievement in achievements:
            count = AchievementModel.objects.filter(user_id=u_id, achievement_id=achievement['id']).count()
            if count == 0:
                model = AchievementModel(user_id=u_id,
                                         achievement_id=achievement['id'],
                                         progress=0,
                                         level=achievement['level'],
                                         value=achievement['value'],
                                         reward=achievement['reward'])
                model.save()

    _create(u_id)

    def _get_value(a_id, item):
        """
        В зависимости от id меняем поведение кода, получая нужные данные
        Args:
            a_id: id достижения
            item: объект модели StaticModel

        Returns:
            data с параметрами
            ВАЖНО: метод всегда должен возвращать либо False, либо словарь с параметрами param1 и param2

        """
        data = {}
        data['param1'] = False

        if item.user_id == u_id:
            if a_id == 3:  # Ачивка "Трудоголик"
                data['param1'] = item.user.statistics.time_online
                data['param2'] = item.value * 3600
            if a_id == 6:  # Ачивка "Бизнесмен"
                data['param1'] = item.user.statistics.budget_total
                data['param2'] = item.value

        return data

    achievements = ACHIEVEMENT_JSON
    # Пройтись по достижениям и проверить, выполнены ли они. Если нет - то обновить прогресс
    for item in achievement_data:
        while True:
            data = _get_value(item.achievement_id, item)
            if data['param1']:
                param1 = data['param1']
                param2 = data['param2']
                res = param1 / param2

                # Код ниже делает таким образом, чтобы при очень больших значениях достижения отрабатывали правильно
                # Например, если пользователь был в сети 50 часов, а достижение требует только 10, потом 30, потом 50,
                # то пользователь всё равно получит награды за все три взятых достижения
                if res > 1:
                    item.level += 1
                    item.user.diamond += item.reward
                    item.value *= achievements[item.achievement_id - 1]['multiple_value']
                    item.reward *= achievements[item.achievement_id - 1]['multiple_reward']

                    item.user.save()
                    item.save()
                else:
                    item.progress = res * 100
                    item.save()
                    break
            else:
                break


def get(u_id):
    """
    Получить список всех достижений для конкретного пользователя
    Args:
        u_id: id пользователя

    Returns:
        Список достижений
    """

    achievements = ACHIEVEMENT_JSON
    for achievement in achievements:
        model = AchievementModel.objects.get(user_id=u_id, achievement_id=achievement['id'])
        description = achievement['description']
        # Подстановка значений
        if '{{ short_value }}' in achievement['description']:
            description = description.replace('{{ short_value }}', str(short_format.short_format(model.value)))
        else:
            description = description.replace('{{ value }}', str(model.value).split('.')[0])

        achievement['description'] = description
        achievement['reward'] = model.reward
        achievement['progress'] = model.progress
        achievement['level'] = model.level

    return achievements
