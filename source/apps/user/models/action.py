from django.contrib.auth.models import AbstractUser
from django.db import models
from .user import UserModel


class ActionModel(models.Model):
    """
    Логгирование действий пользователя и событий, происходящим с ним
    """

    class Meta:
        db_table = "digger_actions"

    owner = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now=True)
    action = models.CharField(max_length=256)
