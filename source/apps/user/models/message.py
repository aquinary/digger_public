from django.apps import apps
from django.db import models

from django.db.models import Q

from ..models import DialogModel, UserModel


class MessageModel(models.Model):
    """
    Личные сообщения пользователей

    Поля:
        author: автор сообщения. Тот, кто отправляет сообщение
        addressee : получатель. Тот, кто получает сообщение
        time: время отправки
        message: отправленное сообщение
        is_read: статус прочтения
    """

    class Meta:
        db_table = "digger_messages"

    dialog = models.ForeignKey(DialogModel, default=None, null=True, related_name='dialog', on_delete=models.CASCADE)
    author = models.ForeignKey(UserModel, default=None, null=True, related_name='author', on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)
    message = models.CharField(db_index=True, max_length=4096)

    def send(self, u_from, u_to, message):
        """
        Отправить сообщение другому игроку
        Так же создаётся диалог в таблице 'digger_dialogs'
        Аргументы:
            u_from: от кого отправляется сообщение
            u_to: кому отправляется сообщение
            message: текст сообщения

        Возвращает:
            Ничего
        """
        dialog_model = apps.get_model('user', 'DialogModel')
        dialog_id = dialog_model.create(self, u_from, u_to)

        model = MessageModel(dialog_id=dialog_id, author_id=u_from,message=message)
        model.save()

    def get_messages(self, u_id, dialog_id):
        """
        Вернусть конкретный диалог

        Аргументы:
            u_id: id пользователя
            dialog_id: id диалога

        Возвращает:
            Список сообщений
        """
        dialog_model = apps.get_model('user', 'DialogModel')

        dialog = dialog_model.objects.filter(Q(Q(u_from_id=u_id) & Q(u_to_id=dialog_id)) |
                                             Q(Q(u_from_id=dialog_id) & Q(u_to_id=u_id)))

        if dialog.count():
            messages = MessageModel.objects.select_related('dialog').filter(dialog_id=dialog.first().id).order_by('-time')

            if dialog.first().u_from_id != u_id:
                for d in dialog:
                    d.status = True
                    d.save()

        else:
            messages = []

        return messages
