import random

from django.db import models
from .user import UserModel

class StatisticModel(models.Model):
    """
    Модель обработки статистики пользователя.
    В ней хранится вся информация пользователя касаемо игровых цифр

    Поля:
        user: ссылка на id пользователя

        Общее:
            signup_date: дата регистрации
            time_total: время с момента регистрации # Поле не создаётся, подсчёт не в БД
            time_online: время в онлайне

        Бюджет:
            budget: текущий бюджет
            budget_total: весь бюджет за всё время
            budget_1m: бюджет через минуту # Поле не создаётся, подсчёт не в БД
            budget_1h: бюджет через час # Поле не создаётся, подсчёт не в БД
            budget_1d: бюджет через сутки # Поле не создаётся, подсчёт не в БД

        Доход:
            income: текущий бюджет
            income_percent_plus: процентный прирост к доходу
            income_percent_minus: процентная убыль к доходу
            income_1m: бюджет через минуту # Поле не создаётся, подсчёт не в БД
            income_1h: бюджет через час # Поле не создаётся, подсчёт не в БД
            income_1d: бюджет через сутки # Поле не создаётся, подсчёт не в БД

        Репутация:
            reputation: текущая репутация
            future_reputation: репутация, которая имеется на данный момент, но ещё не прибавлена к основной
            reputation_income: прирост репутации
            reputation_income_percent_plus: процентный прирост к репутации
            reputation_income_percent_minus: процентая убыль к репутации
            reputation_1m: репутация через минуту # Поле не создаётся, подсчёт не в БД
            reputation_1h: репутация через час # Поле не создаётся, подсчёт не в БД
            reputation_1d: репутация через сутки # Поле не создаётся, подсчёт не в БД

        Другое:
            diamond_change_base: базовый шанс найти алмазы
            diamond_change_percent: процентный пророст к шансу найти алмазы
            diamond_change_percent_minus: процентая убыль к шансу найти алмазы
            diamond_base_count: базовое количество находимых алмазов

            efficiency_time: эффективное время работы (доход 100%) в секундах
            efficiency_recession: спад по времени (доход < 100%) по формуле в зависимости от количества часов
            efficiency: эффективность получаемого дохода в %. 100% соответсвует полному доходу. Фактически, является
            финальным расчётным элементом в игре. Поле не создаётся, подсчёт не в БД
    """
    class Meta:
        db_table = 'digger_statistics'

    user = models.OneToOneField(UserModel, related_name='statistics', null=True, blank=True, on_delete=models.CASCADE)

    signup_date = models.DateTimeField(auto_now_add=True)
    signin_date = models.DateTimeField(auto_now=True)
    last_action = models.DateTimeField(auto_now=True)
    time_online = models.IntegerField(default=0)

    budget = models.DecimalField(max_digits=1000, decimal_places=2, default=1000000)
    budget_total = models.DecimalField(max_digits=1000, decimal_places=2, default=1000000)

    income = models.DecimalField(max_digits=1000, decimal_places=2, default=0)

    reputation = models.DecimalField(max_digits=1000, decimal_places=2, default=0)
    future_reputation = models.DecimalField(max_digits=1000, decimal_places=2, default=0)
    efficiency_time = models.IntegerField(default=32400)
    efficiency_recession = models.IntegerField(default=21600)

    def create(self, u_id):
        model = StatisticModel.objects.create(user_id=u_id)
        model.income = 0
        model.save()

    def get_statistics(self, u_id):
        """
        Получить всю статистику об игроке

        u_id: id пользователя, у которого нужно получить статистику
        """
        model = StatisticModel.objects.get(user_id=u_id)
        return model


