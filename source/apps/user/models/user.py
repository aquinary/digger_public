import time

from django.apps import apps
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q


class UserModel(AbstractUser):
    """
    Работа с пользователями
        Поля:
        username: логин
        password: пароль
        is_active: активность пользователя (если False, то игрок "забанен")
        diamond: количество алмазов
    """
    username = models.CharField(max_length=32, verbose_name='Логин', unique=True)
    password = models.CharField(max_length=256, verbose_name='Пароль')
    avatar = models.CharField(max_length=256, default='default.png')
    is_active = models.BooleanField(default=True)
    diamond = models.IntegerField(default=0)

    class Meta:
        db_table = "digger_users"

    def create(self, username, password):
        """
        Создание пользователя в БД
        Так же создаёт дополнительные записи в других таблицах: статистика
        Args:
            username: логин
            password: пароль

        Returns:
            Ничего
        """
        user = UserModel.objects.create_user(username, None, password)
        statistic_model = apps.get_model('user', 'StatisticModel')

        user.rating_id = user.id
        user.save()
        statistic_model.create(self, user.id)

        return user.id

    def get_userinfo(self, u_id):
        """
        Вернуть информацию о пользователе

        Аргументы:
            u_id: id пользователя

        Возвращает:
            Объект модели UserModel
        """
        return UserModel.objects.filter(id=u_id).first()


def get_online_status(u_id):
    """
    Возвращает онлайн-статус пользователя.
    Args:
        u_id: id пользователя

    Returns:
        Онлайн-статус пользователя
    """

    statistic_model = apps.get_model('user', 'StatisticModel')
    data = statistic_model.objects.filter(Q(user=u_id) | Q(user_id=u_id)).last()

    if data:
        last_action = time.mktime(data.last_action.timetuple())
        now = time.time()

        # Если разница во времени попадает в указанный диапазон, то показать, что он в онлайне
        if now - last_action < 120:
            result = {'online': True}
        else:
            result = {'online': False, 'text': data.last_action}

        return result


def status_update(u_id):
    """
    Пересохраняет поле пользователя, тем самым обновляя значение last_action
    Args:
        u_id: id пользователя

    Returns:
        Ничего
    """
    statistic_model = apps.get_model('user', 'StatisticModel')
    data = statistic_model.objects.get(user=u_id)

    last_action = time.mktime(data.last_action.timetuple())
    now = time.time()

    # Проверяем, была ли какая-нибудь деятельность от пользователя в течении 30 секунд
    if now - last_action < 30:
        data.time_online += int(now - last_action)
    data.save(update_fields=['time_online', 'last_action'])


def get_info(u_id):
    """
    Получить информацию о пользователе: его награды, рейтинг, онлайн-статус
    Args:
        u_id: id пользователя

    Returns:

    """
    user_data = UserModel.objects.filter(Q(username=u_id) | Q(pk=u_id)).last()
    if user_data:
        from ..models import reward
        from ...game.models import rating
    
        user_data.rewards = reward.get(u_id)
        user_data.rating = rating.get(u_id=u_id)
        user_data.online = get_online_status(u_id)

        return user_data


def get_id(page):
    """
    Вернуть id пользователя, если он использует никнейм в URL
    Args:
        page: логин пользовател

    Returns:
        id пользователя
    """
    if page != None:
        if page.isdigit():
            return page
        else:
            data = UserModel.objects.get(username=page)
            return data.id


