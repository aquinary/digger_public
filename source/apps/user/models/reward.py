import json

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q

from .user import UserModel
from ...utils import config


class RewardModel(models.Model):
    """
    Награды пользователей
    user: id пользователя
    time: время получения награды
    reward: id награды
    description: описание награды
    """

    class Meta:
        db_table = "digger_rewards"

    user = models.ForeignKey(UserModel, related_name='reward', on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now=True)
    reward = models.CharField(max_length=256)
    file = models.CharField(max_length=256)
    description = models.CharField(max_length=1024)


def get(u_id):
    """
    Получить список наград пользователя
    Args:
        u_id: id пользователя или его никнейм

    Returns:
        Список наград
    """
    data = RewardModel.objects.filter(Q(user_id=u_id) | Q(user__username=u_id)).select_related('user')

    if data:
        rewards = []
        rewards_list = json.load(open(config.PATHS['reward'], encoding='utf-8'))

        for items in data:
            rewards.append(rewards_list[int(items.reward)])

        return rewards
    return []
