from django import forms


class DialogDetailForm(forms.Form):
    """
    Форма отправки личных сообщений
    fields: список полей, которые будут переданы

    message: сообщение
    password: пароль пользователя
    """
    class Meta:
        fields = ('message', 'addressee',)

    message = forms.CharField(label='', max_length=1024, widget=forms.Textarea)
    addressee = forms.CharField(widget=forms.HiddenInput())

