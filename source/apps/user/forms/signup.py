from django import forms
from django.contrib.auth.hashers import make_password
from django.forms import ModelForm

from ..models.user import UserModel


class SignUpForm(ModelForm):
    class Meta:
        model = UserModel
        fields = ('username', 'password')

    username = forms.CharField(label='', max_length=16, widget=forms.TextInput(attrs={'placeholder': 'Логин'}))
    password = forms.CharField(label='', max_length=32, widget=forms.PasswordInput(attrs={'placeholder': 'Пароль'}))