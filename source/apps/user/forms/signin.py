from django import forms


class SignInForm(forms.Form):
    """
    Форма авторизации на сайте
    fields: список полей, которые будут переданы

    username: логин пользователя
    password: пароль пользователя
    """
    class Meta:
        fields = ('username', 'password')

    username = forms.CharField(label='', max_length=16, widget=forms.TextInput(attrs={'placeholder': 'Логин'}))
    password = forms.CharField(label='', max_length=32, widget=forms.PasswordInput(attrs={'placeholder': 'Пароль'}))
