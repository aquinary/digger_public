import math
import time
from decimal import Decimal

from background_task import background
from django.apps import apps

from ...utils import formula

class Task(object):

    @staticmethod
    @background(schedule=3)
    def process():
        """
        Подсчёт того, сколько необходимо добавить средств в бюджет
        Args:
            u_id: id пользователя
        Returns:
            Ничего
        """
        print('Задача запущена...', end='')
        start = time.time()
        statistic_model = apps.get_model('user', 'StatisticModel')
        statistic_data = statistic_model.objects.all()

        # Обновление бюджета и репутации
        for item in statistic_data:

            last_action = time.mktime(item.last_action.timetuple())
            now = time.time()
            offline_time = now - last_action

            efficiency = formula.efficiency(item)
            # Отсеиваем всех, кто очень долго не заходил в игру и имеет нулевую эффективность
            if efficiency > 0:
                budget = Decimal(item.budget)
                budget_total = item.budget_total
                income = Decimal(item.income)
                future_reputation = item.future_reputation

                # Бюджет - это сумма дохода помноженная на эффективность
                # TODO: в скором времени нужно будет вынести это в formula, так как количество расчётов увеличится
                print (income)
                income *= Decimal(efficiency)
                print (income)
                budget += int((Decimal(income) * Decimal(offline_time)))
                budget_total = budget
                future_reputation = formula.reputation(budget)

                item.budget = budget
                item.budget_total = budget_total
                item.future_reputation = future_reputation
                item.save(update_fields=['budget', 'budget_total', 'future_reputation'])

        end = time.time()-start
        print(f'Задача выполнена за {end}!')

