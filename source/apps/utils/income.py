from decimal import Decimal
from pprint import pprint

from django.apps import apps
# from ..utils import timeit
from source.apps.utils.timeit import timeit


@timeit
def update(u_id):
    """
    Обновить доход пользователя. Просматривает все сформированные бригады и складывает доход от каждой из них
    Args:
        u_id: id пользователя
    """
    data = apps.get_model('game', 'BrigadeModel').objects.filter(user_id=u_id).select_related('user')
    data_mine = apps.get_model('game', 'MineModel').objects.filter(user_id=u_id)

    # Суммирует значение процентов для шахт, чтобы использовать значение далее в вычислениях
    income_list = []
    income = 0
    for dm_item in data_mine:
        for d_item in data:
            if dm_item.mine_id == d_item.mine_id:
                income += d_item.income + ((d_item.income * Decimal(dm_item.percent_income))/100)

    try:
        data[0].user.statistics.income = income
        data[0].user.statistics.save()
    except:
        pass

    # income = 0
    # if data:
    #     for item in data:
    #         income += item.income
    #
    #     data[0].user.statistics.income = income + ((income * Decimal(percent_income))/100)
    #     data[0].user.statistics.save()
