import math


def reputation(budget):
    """
    Формула подсчёта репутации
    Args:
        budget: количество средств, полученных за сессию

    Returns:
        Значение репутации

    """
    return 100 * math.sqrt(budget / 1000000000)