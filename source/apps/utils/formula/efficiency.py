import time


def efficiency(object):
    """
    Рассчитывает эффективность пользователя
    Args:
        object: объект модели StatisticModel

    Returns:
        Эффективность пользователя
    """
    # Расчёт эффиктивности. Чем больше пользователя находится не в сети, тем меньше он получает дохода
    last_action = time.mktime(object.last_action.timetuple())
    now = time.time()
    offline_time = int(now - last_action)

    if offline_time > object.efficiency_time:
        if offline_time < object.efficiency_recession + object.efficiency_time:
            _efficiency = 1 - (100 / object.efficiency_recession * (offline_time - object.efficiency_time)) / 100
        else:
            _efficiency = 0
    else:
        _efficiency = 1

    return _efficiency
