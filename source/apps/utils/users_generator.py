import random

from django.apps import apps


def generate_login():
    with open('resources/data/logins.txt') as file_:
        logins = [login for login in file_]

    import re
    return re.sub("[^a-z0-9]+", "", random.choice(logins), flags=re.IGNORECASE)


def users_generator(count=500):
    """
    Создание пользователей для тестов в БД
    Args:
        count: количество пользователей, которых нужно создать

    Returns:
        Ничего
    """
    user_model = apps.get_model('user', 'UserModel')

    for i in range(count):
        try:
            user = user_model.create(self, username=str(generate_login()), password=str(1))
            print(f'Пользователь {user} был создан!')
        except:
            pass
