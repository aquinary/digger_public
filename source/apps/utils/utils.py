import math
import random

from django.apps import apps
from django.core.paginator import Paginator

def normalize_pagination(page, paginator):
    """
    Нормализовать пагинатор, чтобы он отображал правильные диапазоны

    Аргументы:
        pages: список страниц

    Возвращает:
        Необходимое число смещений
    """
    # Код ниже делает так, чтобы пагинация корректно отображалась
    plusator = 3

    if page == 1 or page == paginator.num_pages:
        plusator = 5
    if page == 2 or page == paginator.num_pages - 1:
        plusator = 4
    if page == 3 or page == paginator.num_pages - 2:
        plusator = 3

    return plusator


def paginator(self, data, page_count=10, u_id=None):
    """
    Делает всё нужное, чтобы на странице отображалась нужная пагинация

    Аргументы:
        data: список данных, которые нужно пагинировать
        page_count: количество отображаемых элементов из списка

    Возвращает:
        Словарь с пагинацией
    """
    page = self.kwargs.get('page', 1)
    paginator = Paginator(data, page_count)

    result = {}
    result['data'] = paginator.get_page(page)

    result['plusator'] = normalize_pagination(page, paginator)
    result['minusator'] = -normalize_pagination(page, paginator)

    return result


def get_rating_page(self, sort, u_id):
    """
    Вернуть страницу рейтинга, на которой находится пользователь
    Args:
        self: ссылка на объект
        sort: группа рейтинга, по которому необходимо получить страницу
        u_id: id пользователя

    Returns:
        Страница в рейтинге
    """
    from ..game.models import rating
    data = rating.get(sort)
    for i, item in enumerate(data):
        if item['user_id'] == u_id:
            if i == 0: i = 1 # Если игрок находится в верхней позиции, то без данного фикса ссылается на 0
            return math.ceil(i / 10)


