import time


def timeit(method):
    def timed(*args, **kwargs):
        time_start = time.time()
        result = method(*args, **kwargs)
        time_end = time.time()
        time_result = (time_end-time_start) * 1000
        print (f'Метод {method.__name__} был выполнен за {time_result:.2f} мс.')
        return result
    return timed