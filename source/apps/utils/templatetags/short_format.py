from decimal import Decimal

from django import template


register = template.Library()

@register.filter(name='short_format')
def short_format(value):
  prefixs = [
    # Слева - количество знаков "до" в числе (не считая самый первый) - 1 000 - 3 знака
    # Справа - символьное обозначение
    {'3': 'k'},
    {'6': 'm'},
    {'9': 'b'},
    {'12': 't'},
    {'15': 'q'},
    {'18': 'u'},
    {'21': 'x'},
    {'24': 'y'},
    {'27': 'h'},
    {'30': 's'},
    {'33': 'd'},
    {'36': 'v'},
    {'39': 'w'},
    {'41': 'r'},
    {'44': 'g'},
    {'47': 'n'},
    {'50': 'c'},
    {'53': 'p'},
    {'56': 'o'},
    {'59': 'z'},
    {'62': 'vi'},
    {'65': 'un'},
    {'68': 'du'},
    {'71': 'tr'},
    {'74': 'qu'},
    {'77': 'qi'},
    {'80': 'se'},
    {'83': 'sp'},
    {'86': 'oc'},
    {'89': 'nv'},
    {'92': 'th'},
    {'95': 'ut'},
    {'98': 'dt'},
    {'101': 'aa'},
    {'104': 'ab'},
    {'107': 'ac'},
    {'110': 'ad'},
    {'113': 'ae'},
    {'116': 'af'},
    {'119': 'ag'},
    {'122': 'ah'},
    {'125': 'ai'},
    {'128': 'aj'},
    {'131': 'ak'},
    {'134': 'al'},
    {'137': 'am'},
    {'140': 'an'},
    {'143': 'ao'},
  ]

  # Отбрасываем дробную часть

  value = int(str(value).split('.')[0])

  # Ниже значения не обрабатываем
  if 999 >= value >= -999:
    return value

  # Ищем соответствие между количеством знаков и постфиксами
  result = []
  for prefix in prefixs:
    numbers = len(str(value)) - 1
    keys = int([*prefix.keys()][-1])

    if numbers >= keys:
      letter = [*prefix.values()][-1]
      number = numbers - int([*prefix.keys()][-1]) + 1

  # Форматируем значение из числа в нужное нам
  # Например, 1000 -> 1.0k, 1433243 -> 1.4m
  value = str(value)
  value = value[:number] + '.' + value[number:number + 1] + letter

  return value
