from django import template
from django.apps import apps

from ...utils import formula
register = template.Library()


@register.filter(name='formula_efficiency')
def formula_efficiency(object):
    """
    Тег для вывода эффективности пользователей в рейтинге
    Обращается к оригинальной формуле и передаёт ей объект модели
    Args:
        object: объект модели
    Returns:
        Эффективность пользователя в %
    """
    statistic_model = apps.get_model('user', "StatisticModel")
    data = statistic_model.objects.get(user=object)
    return f'{formula.efficiency(data) * 100}'


