from decimal import Decimal

from django import template

register = template.Library()

@register.filter(name='multiply')
def multiply(value, multiply):
  """
  Тег выполняет перемножение двух значений value на multiply

  Параметры:
    value: число, которое нужно перемножить
    multiply: множитель

  Возвращает:
    return: произведение value на multiply
"""

  return int(value * multiply)