from django import template

register = template.Library()


@register.filter(name='paginator_offset')
def paginator_offset(value, page_num=1):
    """
    Тег выполняет расчёт смещения для правильного отображения позиций в рейтинге
    Args:
        value: значение текущего счётчика
        page_num: номер страницы

    Returns:
        Смещение для правильного отображения позиций

    """
    offset = 0 if page_num == 1 else 10*(page_num-1)

    return value+offset

