from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Выполняемая команда, которая добавляет в таблицу background_task задачу, которую необходимо выполнить
    TODO: добавить проверку на наличии задачи в базе
    TODO: добавить ключ --force, чтобы избежать проверки
    """
    TASKS_PATH = 'apps.utils.tasks'

    help = f'Файл в формате .py должен находиться по пути {TASKS_PATH}'

    @staticmethod
    def import_from(module, name):
        """
        Метод позволяет делать динамический импорт модулей
        По умолчанию путь строго фиксированный
        Args:
            module: путь к модулю
            name: имя пакета

        Returns:
            Нужный модуль
        """
        module = __import__(module, fromlist=[name])
        return getattr(module, name)

    def handle(self, *args, **options):
        """
        Обработчик команды
        В методе происходит парсинг параметра, переданного через ключ --task
        На основе него происходит импорт модуля из пути TASKS_PATH
        """
        package = self.import_from(self.TASKS_PATH, options['task'])
        package.Task().process(repeat=options['repeat'])

    def add_arguments(self, parser):
        parser.add_argument(
            '--task',
        )
        parser.add_argument(
            '--repeat',
        )
