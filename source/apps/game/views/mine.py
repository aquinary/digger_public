from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from ..models import mine
from ...utils import utils, income


class MineView(TemplateView):
    """
    Обработка страницы шахт
    """
    template_name = "game/mine.htm"

    statistic_model = apps.get_model('user', 'StatisticModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        """
        u_id = request.user.id
        try:
            income.update(u_id)
        except:
            pass
        context = super(MineView, self).get_context_data(**kwargs)
        context['title'] = 'Шахты'

        data = mine.get(u_id)
        context['mines'] = utils.paginator(self, data)
        context['last_price'] = mine.get_last_price(u_id)

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        Строительство шахты
        """
        u_id = request.user.id
        action = request.POST['action']
        mine_id = request.POST['mine_id']
        count = request.POST['count']

        if action == 'build':
            mine.build(u_id)

        if action == 'upgrade':
            mine.upgrade(u_id, mine_id, count)

        income.update(u_id)


        return HttpResponseRedirect('/game/mine/')
