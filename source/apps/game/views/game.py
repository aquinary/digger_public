from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from ...user.models import UserModel


class GameView(TemplateView):
    template_name = "game/game.htm"

    @method_decorator(login_required(login_url='/signin/'))
    # @method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """

        Args:
            request:
            *args:
            **kwargs:

        Returns:

        """
        context = super(GameView, self).get_context_data(**kwargs)
        context['title'] = 'Меню'

        user = UserModel.objects.get(id=request.user.id)
        context['actions'] = user.actionmodel_set.all().order_by('-time')

        return render(request, self.template_name, context)
