from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from ..models import brigade
from ...utils import utils, income


class BrigadeView(TemplateView):
    """
    Обработка страницы шахт
    """
    template_name = "game/brigade.htm"

    statistic_model = apps.get_model('user', 'StatisticModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        """
        u_id = request.user.id
        mine_id = kwargs.get('mine_id', '1')
        context = super(BrigadeView, self).get_context_data(**kwargs)
        context['title'] = 'Бригады'

        data = brigade.get(u_id, mine_id)
        context['brigades'] = utils.paginator(self, data)
        context['last_price'] = brigade.get_last_price(u_id, mine_id)

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        Строительство шахты
        """

        u_id = request.user.id
        action = request.POST.get('action')
        mine_id = request.POST.get('mine_id')
        brigade_id = request.POST.get('brigade_id')
        count = request.POST.get('count')

        if action == 'create':
            brigade.create(u_id, mine_id)
            income.update(u_id)

        if action == 'upgrade':
            brigade.upgrade(u_id, mine_id, brigade_id, count)


        return HttpResponseRedirect('/game/mine/')
