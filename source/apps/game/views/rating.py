from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from ...utils import utils
from ..models import rating

class RatingView(TemplateView):
    """
    Обработка страницы общего рейтинга
    """
    template_name = "game/rating.htm"

    statistic_model = apps.get_model('user', 'StatisticModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    # @method_decorator(cache_page(60 * 5), name='dispatch')
    def get(self, request, *args, **kwargs):
        """

        Args:
            request:
            *args:
            **kwargs:

        Returns:

        """
        context = super(RatingView, self).get_context_data(**kwargs)
        context['title'] = 'Рейтинги'


        data =  rating.get(None)
        context['pages'] = utils.paginator(self, data)

        # Предотвратить множественную переадресацию на саму себя
        if kwargs.get('page', None):
            result = render(request, self.template_name, context)
        else:
            page = utils.get_rating_page(self, None, request.user.id)
            result = redirect(reverse('game:rating', kwargs={'page': page}))

        return result


class RatingBudgetView(TemplateView):
    """
    Страница рейтинга по бюджету
    """
    template_name = "game/rating/budget.htm"

    statistic_model = apps.get_model('user', 'StatisticModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        """

        Args:
            request:
            *args:
            **kwargs:

        Returns:

        """
        context = super(RatingBudgetView, self).get_context_data(**kwargs)
        context['title'] = 'Рейтинги'

        # Пагинация на странице
        context['pages'] = utils.paginator(self, rating.get('budget'))

        # Предотвратить множественную переадресацию на саму себя
        if kwargs.get('page', None):
            result = render(request, self.template_name, context)
        else:
            page = utils.get_rating_page(self, 'budget', request.user.id)
            result = redirect(reverse('game:rating_budget', kwargs={'page': page}))

        return result


class RatingIncomeView(TemplateView):
    """
    Страница рейтинга по доходу
    """
    template_name = "game/rating/income.htm"

    statistic_model = apps.get_model('user', 'StatisticModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        """
        context = super(RatingIncomeView, self).get_context_data(**kwargs)
        context['title'] = 'Рейтинги'

        # Пагинация на странице
        context['pages'] = utils.paginator(self, rating.get('income'))

        # Предотвратить множественную переадресацию на саму себя
        if kwargs.get('page', None):
            result = render(request, self.template_name, context)
        else:
            page = utils.get_rating_page(self, 'income', request.user.id)
            result = redirect(reverse('game:rating_income', kwargs={'page': page}))

        return result


class RatingReputationView(TemplateView):
    """
    Страница рейтинга по репутации
    """
    template_name = "game/rating/reputation.htm"

    statistic_model = apps.get_model('user', 'StatisticModel')
    rating_model = apps.get_model('game', 'RatingModel')

    @method_decorator(login_required(login_url='/signin/'))
    def get(self, request, *args, **kwargs):
        """
        Действия при открытии страницы
        """
        context = super(RatingReputationView, self).get_context_data(**kwargs)
        context['title'] = 'Рейтинги'

        # Пагинация на странице
        context['pages'] = utils.paginator(self, rating.get('reputation'))

        # Предотвратить множественную переадресацию на саму себя
        if kwargs.get('page', None):
            result = render(request, self.template_name, context)
        else:
            page = utils.get_rating_page(self, 'reputation', request.user.id)
            result = redirect(reverse('game:rating_reputation', kwargs={'page': page}))

        return result
