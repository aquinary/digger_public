from decimal import Decimal

from django.apps import apps
from django.db import models

from ...game.models import MineModel
from ...user.models import UserModel


class BrigadeModel(models.Model):
    """
    Бригады

    Fields:
        user: ссылка на пользователя
        mine: привязка бригады к шахте
        brigade_id: id бригады

        icon: иконка брикады
        worker: количество работников
        recruit_cost: стоимость найма бригады
        recruit_worker_cost: стоимость найма сотрудника
        recruit_cost_m: множитель найма бригады
        recruit_worker_cost_m: множитель найма работника
        income: доход
        income_m: множитель дохода
    """

    class Meta:
        db_table = "digger_brigades"

    user = models.ForeignKey(UserModel, related_name='brigade', default=None, null=True, on_delete=models.CASCADE)
    mine_id = models.IntegerField(default=1)
    brigade_id = models.IntegerField(default=1)

    icon = models.CharField(max_length=256, default='default.png')
    worker = models.IntegerField(default=1)
    recruit_cost = models.DecimalField(max_digits=1000, decimal_places=0, default=1)
    recruit_worker_cost = models.DecimalField(max_digits=1000, decimal_places=0, default=1)
    recruit_worker_cost_base = models.DecimalField(max_digits=1000, decimal_places=0, default=10)
    recruit_cost_multiple = models.DecimalField(max_digits=1000, decimal_places=0, default=10)
    recruit_worker_cost_multiple = models.DecimalField(max_digits=1000, decimal_places=0, default=10)
    income = models.DecimalField(max_digits=1000, decimal_places=0, default=1)
    income_base = models.DecimalField(max_digits=1000, decimal_places=0, default=1)
    income_multiple = models.DecimalField(max_digits=1000, decimal_places=0, default=1)


def get_last_price(u_id, mine_id):
    """
    Вернуть последнюю цену для формирования бригады
    Args:
        u_id: id пользователя
        mine_id: id шахты
    Returns:
        Стоимость следующего строительства шахты
    """
    data = BrigadeModel.objects.filter(user_id=u_id, mine_id=mine_id).last()

    if data:
        return data.recruit_cost
    else:
        return 10 * int(mine_id)


def get(u_id, mine_id):
    """
    Вернуть список бригад внутри одной шахты
    Args:
        u_id: id пользователя
        mine_id: id шахты

    Returns:
        Список шахт
    """
    data = BrigadeModel.objects.filter(user_id=u_id, mine_id=mine_id).order_by('-id').values()

    return data


def create(u_id, mine_id):
    """
    Сформировать бригаду
    Args:
        u_id: id пользователя
        mine_id: id шахты

    Returns:
        Ничего, если хватает средств и False, если их не хватает
    """
    mine_id = int(mine_id)
    brigade_data = BrigadeModel.objects.filter(user_id=u_id, mine_id=mine_id).last()
    user_data = apps.get_model('user', 'StatisticModel').objects.get(user_id=u_id)


    if brigade_data:
        brigade_id = brigade_data.brigade_id + 1
        recruit_cost = Decimal(brigade_data.recruit_cost)
        recruit_cost_multiple = Decimal(brigade_data.recruit_cost_multiple)
        recruit_worker_cost = Decimal(brigade_data.recruit_worker_cost)
        recruit_worker_cost_multiple = Decimal(brigade_data.recruit_worker_cost_multiple)
        income_multiple = Decimal(brigade_data.income_multiple)
    else:
        brigade_id = 1
        recruit_cost = 10 * mine_id
        recruit_cost_multiple = 10
        recruit_worker_cost = mine_id
        recruit_worker_cost_multiple = 10
        income_multiple = 1

    if user_data.budget >= recruit_cost:
        # Определяет цену формирования новой шахты
        new_recruit_cost = recruit_cost * recruit_cost_multiple
        # Определяет цену найма сотрудника
        new_recruit_worker_cost = recruit_worker_cost * recruit_worker_cost_multiple
        # Определяет доход бригады
        new_income = brigade_id * mine_id * income_multiple


        model = BrigadeModel(
            user_id=u_id,
            mine_id=mine_id,
            brigade_id=brigade_id,
            recruit_cost=new_recruit_cost,
            recruit_worker_cost=new_recruit_worker_cost,
            recruit_worker_cost_base=new_recruit_worker_cost,
            income=new_income,
            income_base=new_income,
        )

        model.save()

        user_data.budget -= recruit_cost
        user_data.save()


def upgrade(u_id, mine_id, brigade_id, count):
    """
    Улучшить бригаду (добавить сотрудника)
    Args:
        u_id: id пользователя
        mine_id: id шахты
        brigade_id: id бригады
        count: количество улучшений

    Returns:
        Ничего
    """
    count = int(count)

    brigade_data = BrigadeModel.objects.filter(user_id=u_id, mine_id=mine_id, brigade_id=brigade_id).last()
    user_data = apps.get_model('user', 'StatisticModel').objects.get(user_id=u_id)

    if brigade_data:
        recruit_worker_cost_base = Decimal(brigade_data.recruit_worker_cost_base) * count
        recruit_worker_cost = Decimal(brigade_data.recruit_worker_cost) * count
        income = Decimal(brigade_data.income)
        income_base = Decimal(brigade_data.income_base) * count
        worker = brigade_data.worker

        if user_data.budget >= recruit_worker_cost:
            # Определяет стоимость найма следующего сотрудника
            new_recruit_worker_cost = recruit_worker_cost + recruit_worker_cost_base
            # Определяет доход следующей бригады
            new_income = income + income_base
            # Определяет количество рабочиз
            worker += count

            brigade_data.recruit_worker_cost = new_recruit_worker_cost
            brigade_data.income = new_income
            brigade_data.worker = worker
            brigade_data.save()

            user_data.budget -= new_recruit_worker_cost
            user_data.save()

