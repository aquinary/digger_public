import json
from decimal import Decimal

from django.apps import apps
from django.db import models

from ...utils import formula
from source.apps.utils.timeit import timeit
from ...user.models import UserModel


class MineModel(models.Model):
    """
    Шахты

    Fields:
        user_id: владелец шахты
        mine_id: id для отображения правильных в URL

        name: название шахты
        icon: иконка шахты
        depth: глубина
        dig_cost: стоимость следующего копа
        dig_multiple_cost: множитель стоимости следующего копа
        build_cost: стоимость следующего строительства шахты
        build_multiple_cost: множитель строительства следующей шахты
        percent_income: % прибавления дохода (зависит от глубины)
        percent_multiple_income: множитель % дохода
        level: уровнь шахты. Всего три уровня: малая, средняя, большая
    """

    class Meta:
        db_table = "digger_mines"

    user = models.ForeignKey(UserModel, related_name='mine', default=None, null=True, on_delete=models.CASCADE)
    mine_id = models.IntegerField(default=1)

    name = models.CharField(max_length=128, default='Шахта ничего')
    icon = models.CharField(max_length=256, default='default.png')
    depth = models.IntegerField(default=1)
    dig_cost = models.DecimalField(max_digits=1000, decimal_places=0, default=1)
    dig_multiple_cost = models.FloatField(default=2)
    build_cost = models.DecimalField(max_digits=1000, decimal_places=0, default=10)
    build_multiple_cost = models.FloatField(default=2)
    percent_income = models.FloatField(default=0)
    percent_multiple_income = models.FloatField(default=1)
    level = models.IntegerField(default=1)


def get_last_price(u_id):
    """
    Вернуть последнюю цену для строительства шахты
    Args:
        u_id: id пользователя
    Returns:
        Стоимость следующего строительства шахты
    """
    data = MineModel.objects.filter(user_id=u_id).last()

    if data:
        return data.build_cost
    else:
        return 1


@timeit
def get(u_id):
    """
    Вернуть список шахт
    Args:
        u_id: id пользователя

    Returns:
        Список шахт
    """
    data = MineModel.objects.filter(user_id=u_id).order_by('-id').values()

    data_model = apps.get_model('game', 'BrigadeModel')

    for i in data:
        data_b = data_model.objects.filter(user_id=u_id, mine_id=i['mine_id'])
        income = 0

        for item in data_b:
            income += item.income

        i['income'] = income
        i['income_percent'] = formula.income(income, i['percent_income'])
    return data


def build(u_id):
    """
    Построить шахту
    Args:
        u_id: id пользователя

    Returns:
        Ничего, если хватает средств и False, если их не хватает

    """

    MINE_JSON = json.load(open('./resources/data/mine.json', encoding='utf-8'))

    mine_data = MineModel.objects.filter(user_id=u_id).last()

    if mine_data:
        mine_id = mine_data.mine_id + 1
        build_cost = mine_data.build_cost
        build_multiple_cost = mine_data.build_multiple_cost
        name = MINE_JSON[mine_id-1]['name']
    else:
        build_cost = 1
        build_multiple_cost = 10
        name = MINE_JSON[0]['name']
        mine_id = 1

    statistic_data = apps.get_model('user', 'StatisticModel').objects.get(user_id=u_id)
    budget = statistic_data.budget

    if budget >= build_cost:
        new_build_cost = Decimal(build_cost) * Decimal(build_multiple_cost)
        new_dig_cost = Decimal(build_cost)

        model = MineModel(user_id=u_id,
                          mine_id=mine_id,
                          name=name,
                          build_cost=new_build_cost,
                          build_multiple_cost=build_multiple_cost,
                          dig_cost=new_dig_cost,
                          )
        model.save()
        statistic_data.budget -= build_cost
        statistic_data.save()



def upgrade(u_id, mine_id, count=1):
    """
    Копнуть шахту глубже
    Args:
        u_id: id пользователя
        mine_id: id шахты
    """
    count = int(count)
    data = MineModel.objects.filter(user_id=u_id, id=mine_id).first()

    if data:
        dig_cost = data.dig_cost

        dig_multiple_cost = data.dig_multiple_cost
        percent_income = data.percent_income
        percent_multiple_income = data.percent_multiple_income
        depth = data.depth

        statistic_data = apps.get_model('user', 'StatisticModel').objects.get(user_id=u_id)
        budget = statistic_data.budget

        if budget >= dig_cost * count:
            percent_income += percent_multiple_income * count
            depth += 1 * int(count)

            data.percent_income = percent_income
            data.dig_cost = dig_cost * Decimal(dig_multiple_cost)
            data.depth = depth
            data.save()

            statistic_data.budget -= dig_cost * count
            statistic_data.save()

