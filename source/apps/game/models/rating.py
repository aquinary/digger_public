import time
from pprint import pprint

from django.apps import apps
from django.db import models
from django.db.models import Avg, Sum, F

from ...user.models import UserModel


class RatingModel(models.Model):
    """
    Модель для работы с рейтингами пользователей

    Поля:
        # Участвуют в подсчёте с общим рейтингом
        total: общий рейтинг
        income: по доходу
        budget: по бюджету
        reputation: по репутации

        # Не участвуют в подсчёте с общим рейтингом
        mine_depth: по глубине шахты
        worker_count: по количеству рабочих
        playing: по времени игры
        social: по социальной активности
        ref: по приглашённым друзьям

    """

    class Meta:
        db_table = 'digger_ratings'

    user = models.OneToOneField(UserModel, parent_link=True, related_name='ratings', null=True, blank=True,
                                on_delete=models.CASCADE)
    total = models.IntegerField(default=0)
    income = models.IntegerField(default=0)
    budget = models.IntegerField(default=0)
    reputation = models.IntegerField(default=0)

    mine_depth = models.IntegerField(default=0)
    worker_count = models.IntegerField(default=0)
    playing = models.IntegerField(default=0)
    social = models.IntegerField(default=0)
    ref = models.IntegerField(default=0)

    def create(self, u_id):
        """
        Создать запись в БД с рейтингом

        Аргументы:
            u_id: id пользователя

        Возвращает:
            Ничего
        """
        model = RatingModel(user_id=u_id)
        model.save()


def get(sort=None, reverse=True, u_id=None):
    """
    Вернуть список словарей, содержажих полезные данные

    Аргументы:
        TODO: возможно, стоит разделить метод на несколько более мелких
        u_id: id пользователя. Если агрумент присутствует, то возвращает позицию в общем рейтинге для одного игрока
        sort: значение, по которому необходмио сортировать данные
        reverse: если True, то реверсирует выдачю

    Возвращает:
        Список словарей с нужным рейтином
    """
    statistic_model = apps.get_model('user', "StatisticModel")

    # Сортировка данных
    _reverse = '-' if reverse else ''
    _sort = f'{_reverse}{sort}'

    if sort is not None:
        statistic_data = statistic_model.objects.all().select_related('user').order_by(_sort).values(sort,
                                                                                                     'user__username',
                                                                                                     'user_id',
                                                                                                     'user__avatar')
    else:
        statistic_data = statistic_model.objects.annotate(
            total=Avg(F('budget') + F('income') + F('reputation'))).order_by(
            '-total').values('total', 'user__username', 'user_id', 'user__avatar')

    if u_id is not None:
        for i, item in enumerate(statistic_data):
            if int(u_id) == item['user_id']:
                statistic_data = i + 1
                break

    return statistic_data
