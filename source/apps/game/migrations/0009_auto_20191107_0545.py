# Generated by Django 2.2.6 on 2019-11-07 05:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0008_auto_20191107_0213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='minemodel',
            name='dig_multiple_cost',
            field=models.FloatField(default=2),
        ),
    ]
