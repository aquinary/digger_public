# Generated by Django 2.2.6 on 2019-11-11 01:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0018_auto_20191111_0154'),
    ]

    operations = [
        migrations.RenameField(
            model_name='brigademodel',
            old_name='income_m',
            new_name='income_multiple',
        ),
    ]
