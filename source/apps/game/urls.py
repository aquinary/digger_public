from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.GameView.as_view(), name="game"),
    path('rating/', views.RatingView.as_view(), name="rating"),
    path('rating/<int:page>/', views.RatingView.as_view(), name="rating"),
    path('rating/budget/', views.RatingBudgetView.as_view(), name="rating_budget"),
    path('rating/budget/<int:page>/', views.RatingBudgetView.as_view(), name="rating_budget"),
    path('rating/income/', views.RatingIncomeView.as_view(), name="rating_income"),
    path('rating/income/<int:page>/', views.RatingIncomeView.as_view(), name="rating_income"),
    path('rating/reputation/', views.RatingReputationView.as_view(), name="rating_reputation"),
    path('rating/reputation/<int:page>/', views.RatingReputationView.as_view(), name="rating_reputation"),
    path('mine/', views.MineView.as_view(), name="mine"),
    path('mine/page/<int:page>/', views.MineView.as_view(), name="mine"),
    path('mine/<int:mine_id>/', views.BrigadeView.as_view(), name="brigade"),
    path('mine/<int:mine_id>/<int:page>/', views.BrigadeView.as_view(), name="brigade"),
]
